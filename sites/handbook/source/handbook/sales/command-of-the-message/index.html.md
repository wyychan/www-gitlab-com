---
layout: handbook-page-toc
title: "Command of the Message"
description: "GitLab has adopted Force Management's Command of the Message customer value-based sales messaging framework and methodology" 
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

GitLab has adopted and operationalized Force Management's Command of the Message value-based sales messaging methodology. Due to IP constraints, we have migrated content from this page to GitLab's internal Handbook. GitLab team members may access additional content and resources on the [Command of the Message Internal GitLab Handbook page](https://internal-handbook.gitlab.io/sales/command-of-the-message/).

## Customer Value Drivers

Value drivers describe what organizations are likely proactively looking for or needing and are top-of-mind customer topics that exist even if GitLab doesn’t. Value drivers may cause buyers to re-allocate discretionary funds, and they support a value-based customer conversation. Organizations adopt and implement GitLab for the following value drivers:
1.  **Increase Operational Efficiencies** - _simplify the software development toolchain to reduce total cost of ownership_
1.  **Deliver Better Products Faster** - _accelerate the software delivery process to meet business objectives_
1.  **Reduce Security and Compliance Risk** - _simplify processes to comply with internal processes, controls and industry regulations without compromising speed_

## GitLab Differentiators

For now, this content is available on the [Command of the Message Internal GitLab Handbook page](https://internal-handbook.gitlab.io/sales/command-of-the-message/) but will be moved back to the external Handbook soon.
